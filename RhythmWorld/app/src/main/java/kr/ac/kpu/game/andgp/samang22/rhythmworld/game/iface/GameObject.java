package kr.ac.kpu.game.andgp.samang22.rhythmworld.game.iface;

import android.graphics.Canvas;

public interface GameObject {
    public void update();
    public void draw(Canvas canvas);
    public float getPosX();
    public float getPosY();

    public void setPosX(float _x);
    public void setPosY(float _y);
}
