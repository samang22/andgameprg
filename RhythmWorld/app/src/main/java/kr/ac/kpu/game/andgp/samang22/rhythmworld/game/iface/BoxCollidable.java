package kr.ac.kpu.game.andgp.samang22.rhythmworld.game.iface;

import android.graphics.RectF;

public interface BoxCollidable {
    public void getBox(RectF rect);
}
