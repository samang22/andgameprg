package kr.ac.kpu.game.andgp.samang22.rhythmworld.game.iface;

public interface Recyclable {
    public void recycle();
}
