package kr.ac.kpu.game.andgp.samang22.rhythmworld.game.obj;

import android.graphics.Canvas;
import android.graphics.RectF;

import kr.ac.kpu.game.andgp.samang22.rhythmworld.R;
import kr.ac.kpu.game.andgp.samang22.rhythmworld.game.framework.GameWorld;
import kr.ac.kpu.game.andgp.samang22.rhythmworld.game.iface.BoxCollidable;
import kr.ac.kpu.game.andgp.samang22.rhythmworld.game.iface.GameObject;
import kr.ac.kpu.game.andgp.samang22.rhythmworld.game.iface.Recyclable;
import kr.ac.kpu.game.andgp.samang22.rhythmworld.game.world.MainWorld;
import kr.ac.kpu.game.andgp.samang22.rhythmworld.res.bitmap.FrameAnimationBitmap;

public class RobotBBody implements GameObject, BoxCollidable, Recyclable {
    private static final int FRAME_COUNT = 1;
    private static final int FRAME_PER_SECOND = 6;
    private int halfSize;
    private FrameAnimationBitmap fab;
    private float x;
    private float y;
    private static float HEIGHT;
    private boolean isAssembled;

    private RobotBBody() {}


    public static RobotBBody get(float x, float y)
    {
        GameWorld gw = GameWorld.get();
        RobotBBody robotBBody = (RobotBBody)gw.getRecyclePool().get(RobotBBody.class);
        if(robotBBody == null) {
            robotBBody = new RobotBBody();
        }
        robotBBody.fab = new FrameAnimationBitmap(R.mipmap.robot_objb_body, FRAME_PER_SECOND, FRAME_COUNT);
        robotBBody.halfSize = robotBBody.fab.getHeight() / 2;
        robotBBody.x = x;
        robotBBody.y = y;
        robotBBody.isAssembled = false;

        HEIGHT = robotBBody.fab.getHeight();

        return robotBBody;
    }



    @Override
    public void update()
    {
        MainWorld gw = MainWorld.get();
        int hh = fab.getHeight() / 2;

        if (y + hh > gw.getBottom() - RobotALeg.getHeight())
        {
            isAssembled = true;
            gw.remove(this);
            return;
        }


//        x += dx;
//        if(dx > 0 && x > gw.getRight() - halfSize || dx < 0 && x < gw.getLeft() + halfSize)
//        {
//            dx *= -1;
//        }


        y += RobotA.FALLING_SPEED * gw.getTimeDiffInSecond();

    }

    @Override
    public void draw(Canvas canvas) {
        fab.draw(canvas, x, y);
    }

    @Override
    public void getBox(RectF rect) {
        int hw = fab.getWidth() / 2;
        int hh = fab.getHeight() / 2;
        rect.left = x - hw;
        rect.right = x + hw;
        rect.top = y - hh;
        rect.bottom = y + hh;

    }

    @Override
    public void recycle() {

    }

    public static float getHeight()
    {
        return HEIGHT;
    }

    @Override
    public float getPosX()
    {
        return x;
    }
    @Override
    public float getPosY()
    {
        return y;
    }

    @Override
    public void setPosX(float _x) {
        x = _x;
    }

    @Override
    public void setPosY(float _y) {
        y = _y;
    }

    public boolean IsAssembled() {
        return isAssembled;
    }


}
